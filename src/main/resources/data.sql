INSERT INTO user (user_id,username, email) VALUES (1000000,'Ahmad', 'ahmad@santecture.com');
INSERT INTO user (user_id,username, email) VALUES (1000001,'Mohammed', 'mohammed@santecture.com');
INSERT INTO user (user_id,username, email) VALUES (1000002,'Ali', 'ali@santecture.com');
INSERT INTO user (user_id,username, email) VALUES (1000003,'Adam', 'adam@santecture.com');

-- bcrypt password online for password ('p@ssw0rd')
INSERT INTO admin (admin_id, username, password) VALUES (1, 'Admin', '$2a$10$dK7lPylo5hWXaG.jUnZDk.zpjXR1l7l4f4RkuUT6MxG2qv8RPE6Hu');
-- bcrypt password online for password ('ashour@1')
INSERT INTO admin (admin_id, username, password) VALUES (2, 'Admin2', '$2a$10$Jwden4Oqf3WQxC73Xy1tF.hnA.nc54x9Cyw/S90RdzCYKb1FrshUK');


INSERT INTO role (role_id, role_name) VALUES (1, 'ROLE_VIEW');
INSERT INTO role (role_id, role_name) VALUES (2, 'ROLE_ADMIN');

INSERT INTO admin_role (admin_id, role_id) VALUES (1, 2);
INSERT INTO admin_role (admin_id, role_id) VALUES (2, 1);