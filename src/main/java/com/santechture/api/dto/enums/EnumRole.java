package com.santechture.api.dto.enums;

public enum EnumRole {
    ROLE_VIEW, ROLE_ADMIN
}
