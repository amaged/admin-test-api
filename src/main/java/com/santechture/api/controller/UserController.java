package com.santechture.api.controller;


import com.santechture.api.dto.GeneralResponse;
import com.santechture.api.exception.BusinessExceptions;
import com.santechture.api.service.UserService;
import com.santechture.api.validation.AddUserRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@RestController
@RequestMapping(path = "user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('VIEW', 'ADMIN')")
    public ResponseEntity<GeneralResponse> list(Pageable pageable) {
        return userService.list(pageable);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public void addNewUser(@RequestBody AddUserRequest request, HttpServletResponse response) throws BusinessExceptions, IOException {
        userService.addNewUser(request);
        response.sendRedirect("/api/logout");
    }
}
